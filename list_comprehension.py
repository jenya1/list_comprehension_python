
sent = 'I love to eat ice creamin the beach'
upper_s = [word.upper() for word in sent.split()]
first = [word[0] for word in sent.split()]
third = [word[2] for word in sent.split() if len(word) > 2]
lengths = [len(word) for word in sent.split()]
print(upper_s)
print('===================')
print(first)
print('===================')
print(third)
print('====================')
print(lengths)
print('=====================')
power10 = [10*n for n in range(1,11)]
print(power10)
